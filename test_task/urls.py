from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static

from posts.views import PostList

urlpatterns = [
    path('', PostList.as_view(), name='home'),
    path('user/', include('user.urls')),
    path('post/', include('posts.urls')),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
