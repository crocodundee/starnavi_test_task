from posts.models import Post
from rest_framework.authtoken.models import Token

from django.contrib.auth.models import AnonymousUser
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
    DetailView,
    ListView,
    CreateView,
    UpdateView,
    DeleteView
)


class PostDetail(DetailView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context['title'] = 'Post detail'
        context['user'] = user
        if isinstance(user, AnonymousUser):
            token = ""
        else:
            token = Token.objects.get(user=user)
        context['token'] = token
        return context

    def patch(self, request, **kwargs):
        return self.model.get_absolute_path(**kwargs)


class PostList(ListView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Post list'
        return context


class PostCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Post
    fields = ['title', 'description']
    success_url = '/post/{slug}'
    success_message = f'Публикация успешно создана'

    def form_valid(self, form):
        post = form.save(commit=False)
        post.author = self.request.user
        post.save()
        return super(PostCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Post create'
        return context


class PostUpdate(LoginRequiredMixin, UpdateView):
    model = Post
    fields = ['title', 'description']
    template_name = 'posts/post_form.html'
    success_url = '/post/{slug}'


class PostDelete(LoginRequiredMixin, DeleteView):
    model = Post
    template_name_suffix = '_delete'
    success_url = '/post'

