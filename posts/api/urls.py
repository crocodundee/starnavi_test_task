from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from posts.api.views import PostView, PostLikeView

urlpatterns = [                                                                 # METHODS
    path('create/', csrf_exempt(PostView.as_view()), name='api-post-new'),      # post
    path('<slug:slug>/', PostView.as_view(), name='api-post-detail'),           # get
    path('<slug:slug>/update/', PostView.as_view(), name='api-post-update'),    # put
    path('<slug:slug>/delete/', PostView.as_view(), name='api-post-delete'),    # delete
    path('<slug:slug>/like/', PostLikeView.as_view(), name='api-post-like'),    # get
]