from django.shortcuts import get_object_or_404

from rest_framework import status, authentication, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from posts.models import Post
from posts.api.serializers import PostSerializer, PostEditSerializer


class PostView(APIView):
    authentication_classes = (
        authentication.TokenAuthentication,
        authentication.SessionAuthentication
    )

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
    )

    def get(self, request, slug=None):
        """Post detail data"""
        post = get_object_or_404(Post,slug=slug)
        serializer = PostSerializer(post)
        return Response(serializer.data)

    def put(self, request, slug=None):
        """Post update"""
        post = get_object_or_404(Post, slug=slug)
        if self.request.user.id == post.author.id:
            serializer = PostEditSerializer(post, self.request.data)
            data = {}
            if serializer.is_valid():
                serializer.save()
                data = {
                    'message': 'Post updated succesfully',
                    'data': serializer.data
                }
                return Response(data=data)
            return Response(serializer.errors, status=status.HTTP_304_NOT_MODIFIED)
        else:
            data = {'message': 'You can\'t modify this post'}
            return Response(data=data, status=status.HTTP_304_NOT_MODIFIED)

    def delete(self, request, slug=None):
        """Post delete"""
        user = self.request.user
        post = get_object_or_404(Post, slug=slug)
        if post.author.id == user.id:
            post.delete()
            data = {'message': 'Post was deleted'}
            res_status = status.HTTP_200_OK
        else:
            data = {'message': 'Unpermission operation'}
            res_status = status.HTTP_403_FORBIDDEN
        return Response(data=data, status=res_status)

    def post(self, request):
        """Post creating"""
        if self.request.user.is_authenticated:
            post = Post.objects.create(author=self.request.user)
            data = {
                'title': self.request.data['title'],
                'description': self.request.data['description'],
                'author': self.request.user.id,
                'slug': post.slug,
                'likes': post.likes.values_list()
            }
            serialiser = PostSerializer(post, data=data)
            if serialiser.is_valid():
                serialiser.save()
                return Response(serialiser.data, status=status.HTTP_201_CREATED)
            return Response(serialiser.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            data = {
                'message': 'You are not authenticated'
            }
            return Response(data=data, status=status.HTTP_401_UNAUTHORIZED)


class PostLikeView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated, )

    def get(self, request, slug=None):
        """Like/unlike post"""
        post = get_object_or_404(Post, slug=slug)
        serializer = PostSerializer(post)
        user = self.request.user
        if user.is_authenticated:
            if user in post.likes.all():
                post.likes.remove(user)
                data = {
                    'post': serializer.data,
                    'operation': 'unliked'
                }
            else:
                post.likes.add(user)
                data = {
                    'post': serializer.data,
                    'operation': 'liked'
                }
            return Response(data=data, status=status.HTTP_200_OK)
        else:
            data = {
                'message': 'You are not authenticated'
            }
            return Response(data=data, status=status.HTTP_401_UNAUTHORIZED)
