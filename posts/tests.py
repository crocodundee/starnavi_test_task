from django.test import TestCase
from rest_framework.test import APIClient

from user.models import User
from posts.models import Post
from posts.api.serializers import PostSerializer


class TestPostAPI(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            username='test_user',
            password='pwdpwdpwd'
        )
        self.post = Post.objects.create(
            title='Test post',
            description='Testing REST API for Post model',
            author_id=1
        )
        self.post_series = PostSerializer(self.post)
        self.client = APIClient()

    def test_post_detail_view(self, slug='test-post'):
        response = self.client.get(f'http://127.0.0.1:8080/post/api/{slug}/')
        self.assertEqual(response.data, self.post_series.data, response.status_code)

    def test_post_like(self):
        self.client.force_authenticate(user=self.user, token=self.user.auth_token)
        response = self.client.get(self.post.get_api_like_path())
        self.assertEqual(response.data['operation'], 'liked')
        response = self.client.get(self.post.get_api_like_path())
        self.assertEqual(response.data['operation'], 'unliked')

    def test_post_create_view(self):
        self.client.force_authenticate(user=self.user, token=self.user.auth_token)
        data = {
            'title': 'New post title',
            'description': 'New post description'
        }
        response = self.client.post('/post/api/create/', data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Post.objects.count(), 2)

    def test_post_update_view(self):
        data = {
            'title': 'Updated post title',
            'description': 'Updated post description'
        }
        post = Post.objects.get(title='Test post')
        user = User.objects.get(id=post.author.id)
        self.client.force_authenticate(user=user, token=user.auth_token)
        response = self.client.put(f'/post/api/{post.slug}/update/', data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['message'], 'Post updated succesfully')

    def test_post_delete_view(self):
        post = Post.objects.get(title='Test post')
        user = User.objects.get(id=post.author.id)
        self.client.force_authenticate(user=user, token=user.auth_token)
        response = self.client.delete(f'/post/api/{post.slug}/delete/')
        self.assertEqual(response.data['message'], 'Post was deleted')








