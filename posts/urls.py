from django.urls import path, include
from posts.views import (
    PostDetail,
    PostList,
    PostCreate,
    PostUpdate,
    PostDelete,
)


urlpatterns = [
    path('new/', PostCreate.as_view(), name='post-new'),
    path('<slug:slug>/update', PostUpdate.as_view(), name='post-update'),
    path('<slug:slug>/delete', PostDelete.as_view(), name='post-delete'),
    path('<slug:slug>/', PostDetail.as_view(), name='post-view'),
    path('', PostList.as_view(), name='post-list-view'),

    # REST
    path('api/', include('posts.api.urls')),
]