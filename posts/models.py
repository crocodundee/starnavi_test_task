from django.dispatch import receiver
from django.utils import timezone
from django.urls import reverse

from django.db import models
from django.db.models.signals import pre_save

from user.models import User
from slugify import Slugify, CYRILLIC


def slugify(string):
    slug = Slugify(pretranslate=CYRILLIC,
                   to_lower=True,
                   max_length=24,
                   separator='-')
    return slug(string)


class PostQuerySet(models.QuerySet):
    def user_posts(self, user_id):
        return self.all().filter(author=user_id)


class PostModelManager(models.Manager):
    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)

    def all_posts(self, user_id):
        return self.get_queryset().user_posts(user_id)


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=256, verbose_name='Заголовок', unique=True)
    description = models.TextField(verbose_name='Содержание')
    created_at = models.DateField(auto_now=False,
                                  auto_now_add=False,
                                  default=timezone.now(),
                                  verbose_name='Дата публикации')
    slug = models.SlugField(null=True, blank=True)
    likes = models.ManyToManyField(User, blank=True, related_name='post_likes')

    objects = PostModelManager()

    class Meta:
        db_table = 'posts'
        ordering = ['-created_at']

    def __str__(self):
        return self.title

    def get_absolute_path(self, **kwargs):
        return reverse('post-view', kwargs={'slug': self.slug})

    def get_api_like_path(self, **kwargs):
        return reverse('api-post-like', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)


@receiver(pre_save, sender=Post)
def pre_save_post(sender, instance, *args, **kwargs):
    instance.slug = slugify(instance.title)


