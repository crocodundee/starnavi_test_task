from django.contrib import admin
from user.models import User


class UserAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Personal', {'fields': ('first_name', 'last_name', 'image', 'email')}),
        ('Account', {'fields': ('username', 'password')}),
        ('History', {'fields': ('last_login', 'date_joined')}),
        ('Status', {'fields': ('is_superuser', 'is_staff', 'is_active')}),
        ('Permissions', {
            'classes': ('collapse',),
            'fields': ('groups', 'user_permissions')
        })
    )


admin.site.register(User, UserAdmin)

