from django.views.generic import FormView
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import LoginView, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin

from user.forms import LoginForm, SignUpForm
from posts.models import Post


class UserLoginView(LoginView):
    template_name = 'user/login.html'
    authentication_form = LoginForm
    success_url = '/user/profile/'

    def form_valid(self, form):
        authenticate(username=self.request.POST['username'], password=self.request.POST['password'])
        return super(UserLoginView, self).form_valid(form)


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'user/profile.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data()
        context['user'] = self.request.user
        context['posts'] = Post.objects.all_posts(user_id=self.request.user.id)
        return context


class UserRegisterView(SuccessMessageMixin, FormView):
    template_name = 'user/signup.html'
    form_class = SignUpForm
    success_url = '/user/profile/'
    success_message = f'Добро пожаловать на страницу Вашего профиля!'

    def form_valid(self, form):
        user = form.save()
        user.set_password(raw_password=form.cleaned_data['password1'])
        user.save()
        login(self.request, user)
        return super(UserRegisterView, self).form_valid(form)







