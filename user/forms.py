from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from user.models import User


class LoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Логин'}),
                               label='Логин')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Пароль'}),
                               label="Пароль")


class SignUpForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'username'}),
                               label='Логин')
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'password1'}),
                               label="Пароль")
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'password2'}),
                                label="Подтверждение пароля")

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']

    def is_valid(self):
        return (super().is_valid() and (self.cleaned_data.get('password1')==self.cleaned_data.get('password2')))





