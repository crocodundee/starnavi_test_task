from django.test import TestCase
from django.test.client import Client
from parameterized import parameterized as param

from user.models import User
from user.views import ProfileView


register_data = [
    (
        {'username': 'new_user', 'password1': 'new_password', 'password2': 'new_password'},
        302,
    ),
    (
        {'username': 'new_user', 'password1': 'new_password', 'password2': 'bad_password'},
        200,
    ),
]


class TestUserViews(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            username='test_user',
            password='test_pwd'
        )
        self.client = Client()

    def test_get(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_login_register_user(self):
        self.client.login(username=self.user.username, password=self.user.password)
        response = self.client.get('/user/profile/')
        self.assertEqual(response.resolver_match.func.__name__, ProfileView.as_view().__name__)
        self.client.logout()
        response = self.client.get('/user/profile/')
        self.assertRedirects(response, '/user/login/?next=/user/profile/')

    def test_login_unregister_user(self):
        result = self.client.login(username='bad_name', password='bad_pwd')
        self.assertEqual(result, False)

    @param.expand(register_data)
    def test_user_sign_up(self, post_data, exp_status):
        response = self.client.post('/user/sign-up/', data=post_data)
        self.assertEqual(response.status_code, exp_status)



