from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token


class User(AbstractUser):
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    class Meta:
        db_table = 'users'

    def __str__(self):
        return f'{self.username} Profile'



@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
