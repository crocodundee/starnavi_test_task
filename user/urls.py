from django.urls import path
from django.contrib.auth.views import logout_then_login
from .views import UserLoginView, ProfileView, UserRegisterView

urlpatterns = [
    path('login/', UserLoginView.as_view(), name='login'),
    path('logout/', logout_then_login, name='logout'),
    path('sign-up/', UserRegisterView.as_view(), name='sign-up'),
    path('profile/', ProfileView.as_view(), name='profile'),
]
