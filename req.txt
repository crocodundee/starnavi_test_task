asgiref==3.2.3
awesome-slugify==1.6.5
certifi==2019.11.28
chardet==3.0.4
Django==3.0.2
django-crispy-forms==1.8.1
django-templated-mail==1.1.1
djangorestframework==3.11.0
idna==2.8
Jinja2==2.11.0
MarkupSafe==1.1.1
parameterized==0.7.1
Pillow==7.0.0
pytz==2019.3
PyYAML==5.3
regex==2020.1.8
render==1.0.0
requests==2.22.0
screen==1.0.1
sqlparse==0.3.0
Unidecode==0.4.21
urllib3==1.25.8
